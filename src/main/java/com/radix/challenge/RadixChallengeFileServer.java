package com.radix.challenge;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.radix.challenge.dtos.ErrorResponse;
import com.radix.challenge.dtos.PieceInfoResponse;
import com.radix.challenge.dtos.TreeInfoResponse;
import com.radix.challenge.merkle.MerkleBuilder;
import com.radix.challenge.merkle.SimpleMerkleTree;
import org.apache.log4j.BasicConfigurator;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;

public class RadixChallengeFileServer {

    public static void main(String[] args) {

        BasicConfigurator.configure();

        Gson gson =
            new GsonBuilder()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .create();

        Base64.Encoder encoder = Base64.getEncoder();

        MerkleBuilder builder = new MerkleBuilder();

        HashMap<String, SimpleMerkleTree> fileMap = new HashMap<>();

        if (args.length < 1) {

            System.out.println("Missing file parameter");

            return;
        }

        try {

            SimpleMerkleTree tree = builder.fromFile(args[0]);

            fileMap.put(tree.getRootHashHex(), tree);

        } catch (Throwable e) {

            e.printStackTrace();
            return;
        }

        get("/hashes", (request, response) -> {

            ArrayList<TreeInfoResponse> responseData = new ArrayList<>();

            for (Map.Entry<String, SimpleMerkleTree> fileRecord : fileMap.entrySet()) {

                TreeInfoResponse treeInfo = new TreeInfoResponse();

                treeInfo.hash = fileRecord.getKey();
                treeInfo.pieces = fileRecord.getValue().getSize();

                responseData.add(treeInfo);
            }

            return responseData;

        }, gson::toJson);

        get("/piece/:hashId/:pieceIndex", (request, response) -> {

            String hashId = request.params(":hashId");
            String pieceIndex = request.params(":pieceIndex");

            if (!hashId.matches("[0-9a-f]+") || !pieceIndex.matches("[0-9]+")) {

                response.status(400);

                ErrorResponse errorResponse = new ErrorResponse();

                errorResponse.message = "Invalid piece parameters";

                return errorResponse;
            }

            SimpleMerkleTree tree = fileMap.get(hashId);

            if (tree == null) {

                response.status(404);

                ErrorResponse errorResponse = new ErrorResponse();

                errorResponse.message = "Could not find file hash";

                return errorResponse;
            }

            int pieceIndexInt = Integer.parseInt(pieceIndex);

            if (pieceIndexInt < 0 || pieceIndexInt >= tree.getSize()) {

                response.status(404);

                ErrorResponse errorResponse = new ErrorResponse();

                errorResponse.message = "Could not find file piece";

                return errorResponse;
            }

            SimpleMerkleTree.DataNode requestedPiece = tree.getPiece(pieceIndexInt);

            PieceInfoResponse responseData = new PieceInfoResponse();

            responseData.content = encoder.encodeToString(requestedPiece.getData());
            responseData.proof = requestedPiece.getProofs();

            return responseData;

        }, gson::toJson);
    }
}
