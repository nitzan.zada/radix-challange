package com.radix.challenge.dtos;

import java.util.ArrayList;

public class PieceInfoResponse {
    public String content;
    public ArrayList<String> proof;
}
