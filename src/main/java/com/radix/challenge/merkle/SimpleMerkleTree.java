package com.radix.challenge.merkle;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;

public class SimpleMerkleTree {

    private static final byte[] EMPTY_BLOCK_HASH = new byte[32];

    private final MessageDigest _digest;
    private final ArrayList<DataNode> _leaves;
    private final Node _root;
    private final int _size;

    public SimpleMerkleTree(ArrayList<byte[]> pieces, MessageDigest digest, int blockSizeBytes) {

        _leaves = new ArrayList<>();

        int dataNodes = pieces.size();

        int targetHeight = (int) Math.ceil(Math.log(dataNodes) / Math.log(2));

        int overallBlocks = (int) Math.pow(2, targetHeight);

        digest.reset();

        for (int idx = 0; idx < overallBlocks; idx++) {

            SimpleMerkleTree.DataNode newLeaf = new SimpleMerkleTree.DataNode();

            if (idx < dataNodes) {

                newLeaf.data = pieces.get(idx);

                // Data requires padding to compute correct hash
                if (blockSizeBytes != newLeaf.data.length) {
                    newLeaf.hash = digest.digest(Arrays.copyOf(newLeaf.data, blockSizeBytes));
                } else {
                    newLeaf.hash = digest.digest(newLeaf.data);
                }

            } else {
                newLeaf.data = null;
                newLeaf.hash = EMPTY_BLOCK_HASH;
            }

            _leaves.add(newLeaf);
        }

        _root = new SimpleMerkleTree.Node();

        populateNode(_root, _leaves, 0, _leaves.size(), digest);

        _size = dataNodes;
        _digest = digest;
    }

    private void populateNode(SimpleMerkleTree.Node node, ArrayList<SimpleMerkleTree.DataNode> leaves, int start, int end, MessageDigest digest) {

        if (end - start == 2) {

            node.leftNode = leaves.get(start);
            node.leftNode.parent = node;

            node.rightNode = leaves.get(end - 1);
            node.rightNode.parent = node;

            digest.reset();
            digest.update(node.leftNode.hash);
            node.hash = digest.digest(node.rightNode.hash);

        } else {

            node.leftNode = new SimpleMerkleTree.Node();
            node.rightNode = new SimpleMerkleTree.Node();

            node.leftNode.parent = node.rightNode.parent = node;

            populateNode(node.leftNode, leaves, start, start + (end - start) / 2, digest);
            populateNode(node.rightNode, leaves, start + (end - start) / 2, end, digest);

            digest.reset();
            digest.update(node.leftNode.hash);
            node.hash = digest.digest(node.rightNode.hash);
        }
    }

    public int getSize() {
        return _size;
    }

    public String getRootHashHex() {
        return _root.getHashHex();
    }

    public DataNode getPiece(int pieceIndex) {

        if (pieceIndex < 0 || pieceIndex >= _size)
            return null;

        return _leaves.get(pieceIndex);
    }

    public static class Node {

        Node parent;

        byte[] hash;

        Node leftNode;
        Node rightNode;

        public String getHashHex() {

            byte[] rootHash = hash;

            Formatter formatter = new Formatter();

            for (byte b : rootHash) {
                formatter.format("%02x", b);
            }

            return formatter.toString();
        }
    }

    public static class DataNode extends Node {

        byte[] data;

        public byte[] getData() {
            return data;
        }

        public ArrayList<String> getProofs() {

            ArrayList<String> proofs = new ArrayList<>();

            Node last = this;
            Node node = parent;

            while (node != null) {

                if (last == node.rightNode) {
                    proofs.add(node.leftNode.getHashHex());
                } else {
                    proofs.add(node.rightNode.getHashHex());
                }

                last = node;
                node = node.parent;
            }

            return proofs;
        }
    }
}
