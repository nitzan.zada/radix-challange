package com.radix.challenge.merkle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

public class MerkleBuilder {

    public static final int BLOCK_SIZE_BYTES = 1024;

    public SimpleMerkleTree fromFile(String path) throws IOException, NoSuchAlgorithmException {

        File file = new File(path);

        if (!file.exists() || file.isDirectory())
            throw new FileNotFoundException(String.format("File could not be found or is a directory at `%s`", path));

        FileInputStream inputStream = new FileInputStream(file);

        byte[] buffer = new byte[BLOCK_SIZE_BYTES];

        ArrayList<byte[]> pieces = new ArrayList<>();

        int readAmount;

        while ((readAmount = inputStream.read(buffer)) > 0) {

            if (readAmount == BLOCK_SIZE_BYTES) {
                pieces.add(buffer);
            } else {
                pieces.add(Arrays.copyOf(buffer, readAmount));
            }

            buffer = new byte[BLOCK_SIZE_BYTES];
        }

        MessageDigest digest = MessageDigest.getInstance("SHA-256");

        SimpleMerkleTree tree = new SimpleMerkleTree(pieces, digest, BLOCK_SIZE_BYTES);

        return tree;
    }
}
